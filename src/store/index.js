import newsData from '../assets/json/news.json'
import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
export const store = new Vuex.Store({
    state: {
        news: [{}],
    },
    mutations: {
        SET_NEWS: (state, news) => {
            state.news = news;
        }
    },
    actions: {

        /**
         * Here will be API request for get news data
         * This is static data right now for test
         *
         * @param commit
         */
        getNews({commit}){
            commit('SET_NEWS',newsData.news)
        }
    },
    getters: {
        recentNews: state => state.news.slice(1,4),
        allNews: state => state.news
    }
});
