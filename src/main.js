import Vue from 'vue'
import App from './App.vue'
Vue.config.productionTip = false
import '@/assets/bootstrap.min.css'
import { store } from './store'
new Vue({
  render: h => h(App),
  store: store,
}).$mount('#app')
